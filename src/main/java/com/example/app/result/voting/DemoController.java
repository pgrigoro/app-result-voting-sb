package com.example.app.result.voting;

import com.example.app.result.voting.model.ResultVote;
import com.example.app.result.voting.model.Vote;
import com.example.app.result.voting.service.IVoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.*;

@CrossOrigin(maxAge = 3600)
@RestController
@RequestMapping("api")
public class DemoController {

    private IVoteService iVoteService;


    @Autowired
    public DemoController(IVoteService iVoteService)
    {
        this.iVoteService = iVoteService;
    }

    @GetMapping("orders")
    public List<String> getAllOrders()
    {
        List<String> mylist = new ArrayList<String>();
        mylist.add("arg0");
        mylist.add("arg1");
        mylist.add("arg2");
        return mylist;
    }

    @GetMapping("/votes")
    public List<Vote> findAll()
    {
        System.out.println("Start Query Find All");
        List<Vote> result = iVoteService.findAll();
        System.out.println("End Query Find All");
        return result;
    }

    @GetMapping("/votes/result")
    public ResultVote voteResult()
    {
        System.out.println("Start Query Find All");
        List<Vote> lstVote = iVoteService.findAll();
        System.out.println("End Query Find All");
        //SELECT vote, COUNT(id) AS count FROM votes GROUP BY vote
        int sum_Cat = 0;
        int sum_Dog = 0;
        for (Vote vote : lstVote) {
            if(vote.getVotes().toLowerCase().equals("cat")){
                sum_Cat++;
            } else {
                sum_Dog++;
            }
        }
        int total = sum_Cat + sum_Dog;
        String percentCat = "50";
        String percentDog = "50";
        if(total > 0) {
            int cat = Math.round((sum_Cat * 100.0f) / total);
            int dog = 100 - cat;
            percentCat = "" + cat;
            percentDog = "" + dog;
        }
        //Save to result and return
        ResultVote result = new ResultVote();
        result.setPercentCat(percentCat);
        result.setPercentDog(percentDog);
        result.setTotalVotes(total);
        return result;
    }

    @GetMapping("/votes/{lang}")
    public Vote getVote(@PathVariable String lang)
    {
        Vote theVoteContent = iVoteService.findById(lang);

        if(theVoteContent==null)
        {
            throw  new RuntimeException("Content For ID ="+lang +" Not found on DB");
        }
        return  theVoteContent;

    }




}