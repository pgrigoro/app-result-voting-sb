package com.example.app.result.voting.dao;

import com.example.app.result.voting.model.Vote;

import java.util.List;

public interface IVoteDAO {

    public Vote findById(String theLang);
    public List<Vote> findAll();

}
