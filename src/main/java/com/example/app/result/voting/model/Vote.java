package com.example.app.result.voting.model;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "votes")
public class Vote implements Serializable {

    @Id()
    @Column(name = "id")
    private String id;
    @Column(name = "vote")
    private String vote;


    public Vote() {
    }

    public Vote(String id, String votes) {
        this.id = id;
        this.vote = votes;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getVotes() {
        return vote;
    }

    public void setVotes(String votes) {
        this.vote = votes;
    }
}
